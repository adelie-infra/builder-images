##
# Name of directory containing .rpm packages.
# These require a non-disclosure agreement!
#
SDK_NAME := sdk5-1-0

##
# Location in rootfs where SDK installer RPMs
# will be temporarily stored. Arbitrary.
#
SDK_TEMP := /sdk

##
# Location in rootfs where SDK will be installed.
#
SDK_PATH := /usr/local/Cavium_Networks/OCTEON-SDK

##
# Board/chip model to target.
# Derived from 'SDK_PATH/octeon-models.txt'.
#
MY_MODEL := OCTEON_CN78XX

##
# SD card image configuration.
#
#     IMG_SIZE is in gigabytes.
#
IMG_SIZE := 4


all:

##
# Only ask for password once.
#
.PHONY:
sudo:
	@sudo echo Using elevated privileges.

##
# rootfs
#
.PHONY: rootfs-sdk
rootfs-sdk: sudo
	##
	# Build the Docker image.
	# NOTE: Docker BuildKit is utterly unusably broken.
	#
#	DOCKER_BUILDKIT=1 docker build --pull --no-cache . --build-arg SDK_NAME=${SDK_NAME} --build-arg SDK_TEMP=${SDK_TEMP} --output $(CURDIR)/$@
	docker build . --build-arg SDK_NAME=${SDK_NAME} --build-arg SDK_TEMP=${SDK_TEMP} -t $@-${SDK_NAME}

	##
	# Create a .tar from it.
	# We have to start/stop a container to work around
	# the broken '--output' in BuildKit.
	#
	rm -f $(CURDIR)/$@.tar
	t=$$(docker create $@-${SDK_NAME}) \
	;docker export $${t} --output=$(CURDIR)/$@-${SDK_NAME}.tar \
	;docker stop $${t} \
	;docker rm $${t} \
	;

	##
	# Extract the .tar locally.
	#
	sudo rm -fr $(CURDIR)/$@-${SDK_NAME}
	mkdir $(CURDIR)/$@-${SDK_NAME}
	sudo tar -C $(CURDIR)/$@-${SDK_NAME} -xf $(CURDIR)/$@-${SDK_NAME}.tar
	sudo cp /etc/resolv.conf $(CURDIR)/$@-${SDK_NAME}/etc/resolv.conf
	sudo rm -fr $(CURDIR)/$@-${SDK_NAME}/${SDK_TEMP}

	##
	# TODO: recreate .tar with smaller footprint.
	#

##
# disk image
#
.PHONY: sdcard-img
sdcard-img: rootfs-sdk
	##
	# Build the SD card image (work around
	# Docker limitations and fiddly bits).
	#
	# Reason for M*K in 'dd' is performance.
	#
	# NOTE: --privileged is required. Maybe
	# a smaller subset of capabilities can
	# be found, but I can't be bothered.
	#
	dd if=/dev/zero of=$(CURDIR)/${MY_MODEL}-${SDK_NAME}.img bs=1M count=${IMG_SIZE}K
	t=$$(sudo losetup -fP --show $(CURDIR)/${MY_MODEL}-${SDK_NAME}.img) \
	;docker run --privileged \
		-v /dev:/kludge/dev \
		-e SDK_PATH=${SDK_PATH} \
		-e MY_MODEL=${MY_MODEL} \
		-e MY_DRIVE=$${t} \
		--rm -it $<-${SDK_NAME} build-image \
	;sudo losetup -d $${t} \
	;
